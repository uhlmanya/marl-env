(import time)
(import collections)

(import [numpy :as np])
(import [torch :as pt])

(import [agent [Seele Nerv Gehirn]])

(require [hy.contrib.walk [let]])
(require [hy.contrib.loop [loop]])
(require [hy.extra.anaphoric [*]])


