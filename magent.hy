(import [torch :as pt])
(import [torch [nn]])
(import [torch.nn.functional :as F])
(import [torch.distributions [Categorical MultivariateNormal Normal]])
(import [numpy :as np])

(require [hy.contrib.walk [let]])
(require [hy.contrib.loop [loop]])
(require [hy.extra.anaphoric [*]])

(defclass Gehirn []
f"The memory Buffer of a single Agent"
  (defn __init__ [self]
    f"The memory Buffer of a single Agent"
    (setv self.actions      []
          self.states       []
          self.logprobs     []
          self.rewards      []
          self.dones        []
          self.state-values []))

  (defn clear-buffer [self]
    (del (get self.actions      (slice None))
         (get self.states       (slice None))
         (get self.logprobs     (slice None))
         (get self.rewards      (slice None))
         (get self.dones        (slice None))
         (get self.state-values (slice None))))

  (defn get-ordered-trajectories [self &optional n-agents]
    (let [ordered-actions   (.FloatTensor pt)
          ordered-states    (.FloatTensor pt)
          ordered-logprobs  (.FloatTensor pt)
          ordered-rewards   []
          ordered-dones     []
          
          actions (.stack pt self.actions)
          states (.stack pt self.states)
          logprobs (.stack pt self.logprobs) ]

      (for [idx (range (second actions.shape))]
        (if (and (is-not n-agents None) (= n-agents (inc idx)))
          (break)
          (setv ordered-actions  (torch.cat (, ordered-actions  (get actions  (slice None) idx)) 0)
                ordered-states   (torch.cat (, ordered-states   (get states   (slice None) idx)) 0)
                ordered-logprobs (torch.cat (, ordered-logprobs (get logprobs (slice None) idx)) 0))
                
                (-> self.rewards (np.asarray) (get (slice None) idx) (ordered-rewards.extend))
                (-> self.dones   (np.asarray) (get (slice None) idx) (ordered-dones.extend))))
      (, ordered-actions   
         ordered-states    
         ordered-logprobs  
         ordered-rewards   
         ordered-dones))))

(defclass Seele [nn.Module]
  (defn __init__ [self state-size action-size
                 &optional [action-std 0.5]
                           [hidden-size 32]
                           [low-policy-weights-init True]]
    (.__init__ (super))
    (setv self.actor-fc1 (nn.Linear state-size (* 2 hidden-size))
          self.actor-fc2 (nn.Linear (* 2 hidden-size) (* 2 hidden-size))
          self.actor-fc3 (nn.Linear (* 2 hidden-size) hidden-size)
          
          self.actor-μ-head (nn.Linear hidden-size action-size)
          self.actor-σ-head (nn.Linear hidden-size action-size))

    (setv self.critic-fc1 (nn.Linear state-size (* 2 hidden-size))
          self.critic-fc2 (nn.Linear (* 2 hidden-size) (* 2 hidden-size))
          self.critic-fc3 (nn.Linear (* 2 hidden-size) hidden-size)

          self.critic-val (nn.Linear hidden-size 1))

    (setv self.distribution Normal)
    (setv self.action-variance (torch.full (, action-size) (** action-std 2)))

    (when low-policy-weights-init
      (with [_ (.no-grad pt)]
        (self.actor-μ-head.weight.mul_ 0.01))))

  (defn forward [self state]
    (let [a (-> state (self.actor-fc1) (torch.tanh)
                      (self.actor-fc2) (torch.tanh)
                      (self.actor-fc3) (torch.tanh))

          μ (-> a (self.actor-μ-head) (torch.tanh))
          σ (-> a (self.actor-σ-head) (F.softplus))

          v (-> state (self.critic-fc1) (torch.tanh)
                      (self.critic-fc2) (torch.tanh)
                      (self.critic-fc3) (torch.tanh)
                      (self.critic-val)) ]

      (, μ σ v)))

  (defn act [self state]
    (let [(, μ σ v) (self.forward state) 
          var (self.action-var.expand-as μ)
          cov (torch.diag-embed var)
          dist (MultivariateNormal μ σ)
          action (.sample dist)
          log-prob (dist.log-prob action) ]
    
      (, (.detatch action) (.detatch log-prob))))

  (defn eval-std [self state action]
    (let [(, μ σ v) (self.forward state) 
          m (self.distribution (.squeeze μ) (.squeeze σ))
          log-prob (m.log-prob action) ]
      (, log-prob v)))

  (defn evaluate [self state action]
    (let [(, μ σ v) (self.forward state)
          var (self.action-var.expand-as μ)
          cov (torch.diag-embed var)
          dist (MultivariateNormal μ cov)
          action-log-probs (dist.log-prob action)
          dist-entropy (.entropy dist) ]
      (, action-log-probs (torch.squeeze v) dist-entropy))))

(defclass Nerv []
  (defn __init__ [self state-size action-size 
                 &optional [λ 1e-4] [γ 0.99] [ε 0.2]
                           [num-epochs 20] [action-std 0.5]]
    (setv self.state-size    state-size
          self.action-size   action-size
          self.learning-rate λ
          self.gamma         γ
          self.epsilon-clip  ε
          self.num-epochs    num-epochs)

    (setv self.policy      (Seele self.state-size self.action-size action-std 
                                  :hidden-size 128)
          self.policy-prev (Seele self.state-size self.action-size action-std 
                                  :hidden-size 128))

    (setv self.mse-loss (.MSELoss nn)
          self.optim (torch.optim.Adam (.parameters self.policy) 
                                       :lr self.learning-rate 
                                       :betas (, 0.9 0.999)))

    (setv self.episode 0))

  (defn select-action [self state]
    (-> state (torch.FloatTensor) (self.policy-prev.act)))

  (defn update-policy [self memory]
    (let [(, states actions log-probs rewards dones) 
              (memory.get-ordered-trajectories 5) ]
      (setv rewards-discounted []
            reward-discounted 0)

      (for [i (-> rewards (len) (range) (reversed))]
        (when (get dones i)
              (setv reward-discounted 0))
        (setv reward-discounted (+ (get rewards i) (* self.gamma reward-discounted)))
        (rewards-discounted.insert 0 rewards-discounted))

      (setv discounted-rewards (torch.tensor rewards-discounted)
            discounted-rewards (/ (- discounted-rewards (.mean discounted-rewards)) 
                                  (+ (.std discounted-rewards) 1e-5)))

      (setv states (.detatch states)
            actions (.detatch actions)
            log-probs-prev (.detatch log-probs))
            
      (for [epoch (range self.num-epochs)]
        (let [(, log-probs-new state-values dist-entropy) 
                  (self.policy.evaluate states actions) 
              log-probs-next (.squeeze log-probs-new)
              advantages (- discounted-rewards (-> state-values (.detatch) (.squeeze)))
              ratios (torch.exp (- log-probs-next (.detatch log-probs-prev)))
              ratios-clipped (torch.clamp ratios :min (- 1 self.epsilon-clip) 
                                                 :max (+ 1 self.epsilon-clip))
              loss (+ (- (torch.min (* ratios advantages) 
                                    (* ratios-clipped advantages))) 
                      (* 0.5 (self.mse-loss state-values discounted-rewards)) 
                      (- (* 0.01 dist-entropy)))]
          (.zero-grad self.optim)
          (-> loss (.mean) (.backward))
          (.step self.optim)))

      (self.policy-prev.load-state-dict (.state-dict self.policy)))))
