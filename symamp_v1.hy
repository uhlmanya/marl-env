(import logging)
(import [icecream [ic]])

(import [numpy :as np])
(import [scipy [interpolate]])
(import [matplotlib.pyplot :as plt])

(import [gym.spaces [Box Discrete]])
(import [pettingzoo [ParallelEnv]])
(import [pettingzoo.utils [wrappers from-parallel agent-selector]])

(import [PySpice.Probe.Plot [plot]])
(import [PySpice.Plot.BodeDiagram [bode-diagram]])
(import [PySpice.Spice.Netlist [Circuit SubCircuitFactory]])
(import [PySpice.Spice.Library [SpiceLibrary]])
(import [PySpice.Unit [*]])

(require [hy.contrib.walk [let]]) 
(require [hy.contrib.loop [loop]])
(require [hy.extra.anaphoric [*]])
(require [hy.contrib.sequences [defseq seq]])
(import [hy.contrib.sequences [Sequence end-sequence]])

(defclass SymAmp [SubCircuitFactory]
  (setv NAME "symmetrical_amplifier"
        NODES (, "REF" "INP" "INN" "OUT" "SS" "DD"))
  (defn __init__ [self]
    (.__init__ (super))
    ;; Biasing
    (self.MOSFET "N0" "REF" "REF" "SS" "SS" :model "nmos" :w 2.9e-5 :l 0.8e-6)
    (self.MOSFET "N1" "A"   "REF" "SS" "SS" :model "nmos" :w 1.6e-5 :l 0.8e-6)
    ;; Differential Pair
    (self.MOSFET "N2" "B"   "INN" "A"  "SS" :model "nmos" :w 1.0e-6 :l 1.1e-6)
    (self.MOSFET "N3" "C"   "INP" "A"  "SS" :model "nmos" :w 1.0e-6 :l 1.1e-6)
    ;; PMOS Current Mirrors
    (self.MOSFET "P0" "B"   "B"   "DD" "DD" :model "pmos" :w 1.8e-6 :l 1.0e-6)
    (self.MOSFET "P1" "D"   "B"   "DD" "DD" :model "pmos" :w 7.2e-6 :l 1.0e-6)
    (self.MOSFET "P2" "C"   "C"   "DD" "DD" :model "pmos" :w 1.8e-5 :l 1.0e-6)
    (self.MOSFET "P3" "OUT" "C"   "DD" "DD" :model "pmos" :w 7.2e-5 :l 1.0e-6)
    ;; NMOS Current Mirror
    (self.MOSFET "N4" "D"   "D"   "SS" "SS" :model "nmos" :w 1.1e-5 :l 1.6e-6)
    (self.MOSFET "N5" "OUT" "D"   "SS" "SS" :model "nmos" :w 1.1e-5 :l 1.6e-6)))

(defclass SymAmpEnv [ParallelEnv]
  (setv metadata {"render.modes" ["human"]
                  "name" "symamp_v1"})

  (defn __init__ [self max-cycles 
                 &optional [lib-path "./lib/90nm_bulk.lib"]
                           [w-min 500e-9]    [w-max 100e-6]
                           [l-min 150e-9]    [l-max  50e-6]
                           [A0dB-min -150.0] [A0dB-max 150.0]
                           [PM-min -180.0]   [PM-max 180.0]
                           [GM-min -150.0]   [GM-max 150.0]
                           [f3dB-min 1.0]    [f3dB-max 1.0e11]
                           [fug-min 1.0]     [fug-max 1.0e11]]

    (setv self.w-min w-min
          self.w-max w-max
          self.l-min l-min
          self.l-max l-max)

    (setv self.A0dB-min A0dB-min
          self.A0dB-max A0dB-max
          self.PM-min   PM-min
          self.PM-max   PM-max
          self.GM-min   GM-min
          self.GM-max   GM-max
          self.f3dB-min f3dB-min
          self.f3dB-max f3dB-max
          self.fug-min  fug-min
          self.fug-max  fug-max)

    (setv self.spice-lib (SpiceLibrary lib-path))
    (setv self.netlist (Circuit "symamp_v1"))
    (self.netlist.include (get self.spice-lib "nmos"))

    (self.netlist.subcircuit (SymAmp))
    (self.netlist.X "OP" "symmetrical_amplifier" "R" "P" "N" "O" 0 "DD")

    (setv self.op-amp (first self.netlist.subcircuits))

    (setv self.biasing-current (self.netlist.I "bias" 0 "R" (u-uA 10.0))
          self.supply-voltage (self.netlist.VoltageSource "dd" "DD" 0 (u-V 1.2))
          self.load-capacitance (self.netlist.C "L" "O" 0 (u-pF 10.0)))

    (setv self.non-inverting-input (self.netlist.VoltageSource "ip" "P" 0 (u-V 0.6))
          self.inverting-input (self.netlist.SinusoidalVoltageSource "in" "N" "E" 
                                                                     :dc-offset (u-V 0.0) 
                                                                     :ac-magnitude (u-V -1.0))
          self.output-buffer (self.netlist.VoltageControlledVoltageSource "in" "E" 0 "O" 0 (u-V 1.0)))

    (setv self.max-steps max-cycles
          self.num-nmos 6
          self.num-pmos 4)

    (setv self.possible-agents (+ (lfor i (range self.num-nmos) f"nmos_{(str i)}")
                                  (lfor i (range self.num-pmos) f"pmos_{(str i)}"))

          self.agent-device-mapping (dfor ag self.possible-agents 
                                          [ag (+ "M" 
                                                 (-> ag (first) (.upper))
                                                 (-> ag (.split "_") (last)))]))

    (setv self.action-spaces (dfor agent self.possible-agents 
                                   [agent (Box (np.array [0 0] :dtype np.float) 
                                               (np.array [1 1] :dtype np.float)
                                               :dtype np.float)]))

    (setv self.observation-spaces (dfor agent self.possible-agents
                                        [agent (Box (np.zeros 5 :dtype np.float)
                                                    (np.array (list (repeat Inf 5)) :dtype np.floa)
                                                    ;(np.ones 5 :dtype np.float)
                                                    :dtype np.float)])))
  
  (defn render [self &optional [mode "human"]]
    (print self.netlist))

  (defn close [self]
    ; DESTROY NETLIST / SIMULATOR / PYSPICE OBJECT
    pass)

  (defn reset [self]
    (setv self.simulator (self.netlist.simulator :simulator "ngspice-subprocess"
                                                 :temperature 27
                                                 :nominal-temperature 27))

    (setv self.agents (get self.possible-agents (slice None))
          self.moves 0)
    (dfor agent self.agents [agent (np.random.rand 5)]))

  (defn simulate [self]
    (let [_ (logging.disable logging.FATAL)
          ac-analysis (self.simulator.ac :start-frequency (u-Hz self.fug-min) 
                                         :stop-frequency  (u-Hz self.fug-max) 
                                         :number-of-points 10
                                         :variation "dec")

          freq (-> ac-analysis (. frequency) (np.array))
          gain (- (-> ac-analysis (get "O") (np.absolute) (np.log10) (* 20))
                  (-> ac-analysis (get "N") (np.absolute) (np.log10) (* 20)))
          phase (- (-> ac-analysis (get "O") (np.angle :deg True))
                   (-> ac-analysis (get "N") (np.angle :deg True)))
          _ (logging.disable logging.NOTSET)

          gf ((juxt #%(get gain %1) #%(get freq %1)) (np.argsort gain))
          pf ((juxt #%(get phase %1) #%(get freq %1)) (np.argsort phase))

          A0dB (interpolate.pchip-interpolate freq gain [1.0])
          A3dB (- A0dB 3)
          f3dB (interpolate.pchip-interpolate #* gf [A3dB])

          fug  (if (> A0dB 0) 
                   (interpolate.pchip-interpolate #* gf [0.0])
                   (np.array self.fug-min))
          fp0  (interpolate.pchip-interpolate #* pf [0.0])
          PM   (if (> A0dB 0)
                   (interpolate.pchip-interpolate freq phase [fug])
                   (np.zeros 1))
          GM   (interpolate.pchip-interpolate freq gain [fp0])]
      (tuple (map #%(.item %1) [A0dB f3dB fug PM GM]))))

  (defn step [self actions]
    (if (not actions)
        (, {} {} {} {})
        (let [_ (for [(, agent action) (.items actions)]
                  (let [dev (self.op-amp.element (get self.agent-device-mapping agent))
                        w (-> action (first) 
                                     (* (- self.w-max self.w-min)) 
                                     (+ self.w-min))
                        l (-> action (second) 
                                     (* (- self.l-max self.l-min)) 
                                     (+ self.l-min))]
                    (setv dev.width  w
                          dev.length l)))
              (, A0dB f3dB fug PM GM) (self.simulate)
              spec-reached False

              rewards (dfor agent self.agents 
                            [agent 7])

              design-done (or (> self.moves self.max-steps) 
                              spec-reached )
              dones (dfor agent self.agents
                          [agent design-done])
              
              observations (dfor agent self.agents
                                 [agent (np.array [(/ (- A0dB self.A0dB-min) (- self.A0dB-max self.A0dB-min))
                                                   (/ (- f3dB self.f3dB-min) (- self.f3dB-max self.f3dB-min))
                                                   (/ (- fug self.fug-min) (- self.fug-max self.fug-min))
                                                   (/ (- PM self.PM-min) (- self.PM-max self.PM-min))
                                                   (/ (- GM self.GM-min) (- self.GM-max self.GM-min))])])

              infos (dfor agent self.agents
                          [agent {}])]

          (setv self.moves (inc self.moves))
          (, observations rewards dones infos)))))

(defn parallel-env [max-cycles]
  (SymAmpEnv max-cycles))

(defn raw-env [max-cycles]
  (-> (SymAmpEnv max-cycles) (from-parallel)))

(defn env [max-cycles]
  (-> (raw-env max-cycles) 
      (wrappers.CaptureStdoutWrapper) 
      (wrappers.OrderEnforcingWrapper)))
