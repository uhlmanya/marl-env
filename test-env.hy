(import [pettingzoo.test [api-test
                          parallel-api-test 
                          seed-test
                          max-cycles-test
                          render-test
                          performance-benchmark
                          test-save-obs]])
(import symamp_v1)

(print f"Running API Test ...\n")
(setv env (.env symamp_v1 100))
(api-test env :num-cycles 10 :verbose-progress True)
(print f"Done!\n")

;(print f"Running Max Cycles Test ...\n")
;(max-cycles-test symamp_v1)
;(print f"Done!\n")

(print f"Running Performance Benchmark ...\n")
(performance-benchmark env)
(print f"Done!\n")

(print f"Running Observation Saving Test ...\n")
(test-save-obs env)
(print f"Done!\n")

;(print f"Running Render Test ...\n")
;(render-test env)
;(print f"Done!\n")

;(print f"Running Seed Test ...\n")
;(setv env-fn symamp_v1.env)
;(seed-test env-fn :num-cycles 10)
;(print f"Done!\n")

(print f"Running Parallel Test ...\n")
(setv par-env (.parallel-env symamp_v1 100))
(parallel-api-test par-env :num-cycles 10)
(print f"Done!\n")
